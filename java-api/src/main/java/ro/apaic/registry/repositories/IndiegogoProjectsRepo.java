package ro.apaic.registry.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ro.apaic.registry.enitites.IndiegogoProject;

import java.util.Set;

public interface IndiegogoProjectsRepo extends CrudRepository<IndiegogoProject, Integer> {
    @Query("from IndiegogoProject i where i.backers >= 20")
    Set<IndiegogoProject> findAll();
}
