package ro.apaic.registry.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ro.apaic.registry.enitites.CreatorNoProjects;
import ro.apaic.registry.enitites.KickstarterProject;

import java.util.Set;

public interface KickstarterProjectsRepo extends CrudRepository<KickstarterProject, Integer> {
    Set<KickstarterProject> findAll();
    Set<KickstarterProject> findAllByCreatorId(double creatorId);

    @Query("from KickstarterProject k where k.pledged >= k.goal and k.goal >= 100000")
    Set<KickstarterProject> findAllSuccessfull();

    @Query("from KickstarterProject k where k.pledged < k.goal and k.goal >= 100000")
    Set<KickstarterProject> findAllUnuccessfull();

    @Query("from KickstarterProject k where (select count(creatorId) from KickstarterProject where creatorId = k.creatorId) > 1")
    Set<KickstarterProject> findAllRelated();

    @Query("SELECT new ro.apaic.registry.enitites.CreatorNoProjects(k.creatorId, k.creator, count(k), sum(k.pledged), " +
            "(select count(k1) from KickstarterProject k1 where k.creatorId = k1.creatorId and k1.pledged >= k1.goal) as success, " +
            "(select count(k1) from KickstarterProject k1 where k.creatorId = k1.creatorId and k1.pledged < k1.goal) as fail) " +
                        "FROM KickstarterProject k " +
                        "GROUP BY k.creatorId, k.creator " +
                        "ORDER BY 3 desc")
    Set<CreatorNoProjects> findCreatorsNoProjects();
}
