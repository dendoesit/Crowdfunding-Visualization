package ro.apaic.registry.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import ro.apaic.registry.enitites.KickstarterProject;
import ro.apaic.registry.repositories.KickstarterProjectsRepo;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import java.util.Set;

public class DBUtils {

    public static void persistEntities(Set entities, CrudRepository repository) {
        entities.forEach(entity -> {
            repository.save(entity);
        });
    }
}
