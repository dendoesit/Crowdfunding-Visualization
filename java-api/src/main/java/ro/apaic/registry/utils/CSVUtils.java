package ro.apaic.registry.utils;

import ro.apaic.registry.enitites.IndiegogoProject;
import ro.apaic.registry.enitites.KickstarterProject;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.util.*;
import java.util.stream.Collectors;

public class CSVUtils {

    public static Set<KickstarterProject> getKickstarterProjectsFromCSV(InputStream file) throws IOException {

        List<String> lines = readAllLines(file);

        Set<KickstarterProject> values = lines.stream()
                .map(line -> Arrays.asList(line.split(",")))
                .map(list -> KickstarterProject.builder()
                        .link(list.get(0))
                        .backers(Double.parseDouble(list.get(1)))
                        .category(list.get(2))
                        .city(list.get(3))
                        .creator(list.get(4))
                        .creatorId(Double.parseDouble(list.get(5)))
                        .duration(Double.parseDouble(list.get(6)))
                        .goal(Double.parseDouble(list.get(7)))
                        .pledged(Double.parseDouble(list.get(8)))
                        .projectName(list.get(9))
                        .state(list.get(10))
                        .build())
                .collect(Collectors.toSet());

        return values;
    }

    private static List<String> readAllLines(InputStream file) {
        List<String> result = new ArrayList<>();
        try {
            CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
            decoder.onMalformedInput(CodingErrorAction.IGNORE);
            InputStreamReader reader = new InputStreamReader(file, decoder);
            BufferedReader bufferedReader = new BufferedReader(reader);
            StringBuilder sb = new StringBuilder();
            String line = bufferedReader.readLine();
            while (line != null) {
                result.add(line);
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static Set<IndiegogoProject> getIndiegogoProjectsFromCSV(InputStream file) throws IOException {
        //Path path = Paths.get("arbitoryPath");

        List<String> lines = readAllLines(file);

        Set<IndiegogoProject> values = lines.stream()
                .map(line -> Arrays.asList(line.split(",")))
                .map(list -> IndiegogoProject.builder()
                        .link(list.get(0))
                        .backers(Double.parseDouble(list.get(1)))
                        .category(list.get(2))
                        .creator(list.get(3))
                        .goal(Double.parseDouble(list.get(4)))
                        .pledged(Double.parseDouble(list.get(5)))
                        .projectName(list.get(6))
                        .build())
                .collect(Collectors.toSet());

        return values;
    }
}