package ro.apaic.registry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import ro.apaic.registry.enitites.IndiegogoProject;
import ro.apaic.registry.enitites.KickstarterProject;
import ro.apaic.registry.repositories.IndiegogoProjectsRepo;
import ro.apaic.registry.repositories.KickstarterProjectsRepo;
import ro.apaic.registry.utils.CSVUtils;
import ro.apaic.registry.utils.DBUtils;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;

@SpringBootApplication
public class RegistryApplication {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(RegistryApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder().build();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }
}
