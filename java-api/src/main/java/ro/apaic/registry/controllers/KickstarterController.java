package ro.apaic.registry.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ro.apaic.registry.enitites.CreatorNoProjects;
import ro.apaic.registry.enitites.IndiegogoProject;
import ro.apaic.registry.enitites.KickstarterProject;
import ro.apaic.registry.repositories.IndiegogoProjectsRepo;
import ro.apaic.registry.repositories.KickstarterProjectsRepo;
import ro.apaic.registry.utils.CSVUtils;
import ro.apaic.registry.utils.DBUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.Set;

@RestController
public class KickstarterController {
    private static String resourcesDir = "D:\\Workspace\\Java\\API Projects\\src\\main\\resources\\";
    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private KickstarterProjectsRepo kickstarterProjectsRepo;

    @Autowired
    private IndiegogoProjectsRepo indiegogoProjectsRepo;

    @GetMapping("/loadEvToDB")
    public void loadevToDB() throws IOException {
        loadDataInDB();
    }

    @GetMapping("/kickstarterAllProjects")
    public ResponseEntity<Set<KickstarterProject>> getKickstarterProjects() {
        return new ResponseEntity<>(kickstarterProjectsRepo.findAll(), HttpStatus.OK);
    }

    @GetMapping("/kickstarterAllSuccessfullProjects")
    public ResponseEntity<Set<KickstarterProject>> getKickstarterAllSuccessfullProjects() {
        return new ResponseEntity<>(kickstarterProjectsRepo.findAllSuccessfull(), HttpStatus.OK);
    }

    @GetMapping("/kickstarterAllUnsuccessfullProjects")
    public ResponseEntity<Set<KickstarterProject>> getKickstarterAllUnsuccessfullProjects() {
        return new ResponseEntity<>(kickstarterProjectsRepo.findAllUnuccessfull(), HttpStatus.OK);
    }


    @GetMapping("/kickstarterRelated")
    public ResponseEntity<Set<KickstarterProject>> getKickstarterRelated() {
        return new ResponseEntity<>(kickstarterProjectsRepo.findAllRelated(), HttpStatus.OK);
    }

    @PostMapping("/addCoords")
    public void addCoords(@RequestParam int id, @RequestParam String lat, @RequestParam String lon) {
        KickstarterProject kickstarterProject = kickstarterProjectsRepo.findById(id).get();

        kickstarterProject.setLat(lat);
        kickstarterProject.setLon(lon);

        kickstarterProjectsRepo.save(kickstarterProject);
    }

    @GetMapping("/kickstarterCreators")
    public ResponseEntity<Set<CreatorNoProjects>> getKickstarterCreators() {
        return new ResponseEntity<>(kickstarterProjectsRepo.findCreatorsNoProjects(), HttpStatus.OK);
    }

    @GetMapping("/kickstarterProjectsByCreatorId")
    public ResponseEntity<Set<KickstarterProject>> getKickstarterProjectsByCreatorId(@RequestParam double creatorId) {
        return new ResponseEntity<>(kickstarterProjectsRepo.findAllByCreatorId(creatorId), HttpStatus.OK);
    }

    private void loadDataInDB() throws IOException {
        ClassPathResource res = new ClassPathResource("above100k.csv");
        Set<KickstarterProject> kickstarterProjectSet = CSVUtils.getKickstarterProjectsFromCSV(res.getInputStream());
        res = new ClassPathResource("related.csv");
        kickstarterProjectSet.addAll(CSVUtils.getKickstarterProjectsFromCSV(res.getInputStream()));
        DBUtils.persistEntities(kickstarterProjectSet, kickstarterProjectsRepo);

        res = new ClassPathResource("indie100.csv");
        Set<IndiegogoProject> indiegogoProjectSet = CSVUtils.getIndiegogoProjectsFromCSV(res.getInputStream());
        DBUtils.persistEntities(indiegogoProjectSet, indiegogoProjectsRepo);
    }

}