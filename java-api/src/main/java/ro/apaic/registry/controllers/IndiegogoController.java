package ro.apaic.registry.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.apaic.registry.enitites.IndiegogoProject;
import ro.apaic.registry.repositories.IndiegogoProjectsRepo;

import java.util.Set;

@RestController
public class IndiegogoController {

    @Autowired
    private IndiegogoProjectsRepo indiegogoProjectsRepo;

    @GetMapping("/indiegogoProjects")
    public ResponseEntity<Set<IndiegogoProject>> getGrades() {
        return new ResponseEntity<>(indiegogoProjectsRepo.findAll(), HttpStatus.OK);
    }

}
