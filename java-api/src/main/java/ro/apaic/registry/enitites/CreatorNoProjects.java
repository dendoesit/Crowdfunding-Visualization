package ro.apaic.registry.enitites;

import lombok.*;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor
public class CreatorNoProjects {

    private double creatorId;
    private String creator;
    private long noProjects;
    private double money;
    private long noSuccess;
    private long noFails;
}