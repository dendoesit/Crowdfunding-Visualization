package ro.apaic.registry.enitites;

import lombok.*;

import javax.persistence.*;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor
@Entity
@Table(name = "kickstarter")
public class KickstarterProject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String link;
    private double backers;
    private String category;
    private String city;
    private String creator;
    private double creatorId;
    private double duration;
    private double goal;
    private double pledged;
    private String projectName;
    private String state;

    private String lon = "0";
    private String lat = "0";
}