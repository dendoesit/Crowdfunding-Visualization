# -*- coding: utf-8 -*-
"""
Created on Thu Jun  7 14:26:00 2018

@author: Popoutanu
"""


from numpy import *
import numpy as np
import builtins
import json
import pandas as pd
#file = 'data1.json'
import os
import re

def get_items_from_file(filename):
    temp_list= []
    if re.match('Indiegogo', filename):
        print(filename)
        data = pd.DataFrame(pd.read_json(filename,lines=True)['data'])
        #for each file get values
        #print(data)
        for key,value in data.items():
            line= value
            for x in line:
                currency = ""
                try:
                    currency = x['currency_code']
                except Exception as e:
                    try:
                        currency = x['currency']
                    except Exception as e1:
                        print("Currency err for project", x)
                # get only projects in the UK and transform balance into numerical value
                if (currency == 'GBP'  and int(re.sub("[^0-9]", "",x['balance'])) > 99900):
                    print('proiectul ', x['title'])
                    #data to be saved
                    try:
                       # x['location']
                        duplicate = False
                        for a in itemslist:
                            if x['title'] == a['project name'] :
                                if x['collected_percentage'] == a['pledged']:
                                    duplicate = True
                                    break
                                else:
                                    x = { "project name": x['title'], "goal" :re.sub("[^0-9]", "",x['balance']), "pledged":x['collected_percentage'], "category": x["category_name"],"creator":x["partner_name"], "backers":x["cached_collected_pledges_count"],  "Link": x['url']}
                        if duplicate == False:
                            x = { "project name": x['title'], "goal" :re.sub("[^0-9]", "",x['balance']), "pledged":x['collected_percentage'], "category": x["category_name"],"creator":x["partner_name"], "backers":x["cached_collected_pledges_count"],  "Link": x['url']}
                            temp_list.append(x)
                    except Exception as e:
                        print("This is an error message!", e)
    print('this is the temporary list for file', filename, temp_list ,len(temp_list), '\n')
    return temp_list

itemslist = []
#get all files from folder
for filename in os.listdir(os.getcwd()):
    #print("before adding from file", filename, "itemslist is: \n\t", itemslist)
    itemslist.extend(get_items_from_file(filename))
                        
with builtins.open('indie.json','w') as outfile:
    final_list = itemslist
    print('final list' ,len(final_list))
    json.dump(final_list, outfile)
#np.savetxt('data.csv', (col1_array, col2_array, col3_array), delimiter=',')


# Converting json into CSV that can be used further on in a database.
with open('indie.json','r') as file:
    print(file)
    #df = pd.read_json(file)
    #print(df)
    #df.to_csv('above100k.csv',index=False)