# -*- coding: utf-8 -*-
"""
Created on Thu Jun  7 14:26:00 2018

@author: Popoutanu
"""


from numpy import *
import numpy as np
import builtins
import json
import pandas as pd
#file = 'data1.json'
import os
import re

def get_items_from_file(filename):
    temp_list= []
    if re.match('Kickstarter', filename):
        print(filename)
        data = pd.DataFrame(pd.read_json(filename,lines=True)['data'])
        #for each file get values
        #print(data)
        for key,value in data.items():
            line= value
            for x in line:
                #conditionscountry = ""
                try:
                    country = x['location']["country"]
                except Exception as e:
                    try:
                        country = x['country']
                    except Exception as e1:
                        print("Country err for project", x)
                if (country == 'GB'  and x['goal'] > 99000 and x['pledged'] > x['goal']):
                    print('proiectul ', x['name'])
                    #data to be saved
                    try:
                       # x['location']
                        duplicate = False
                        for a in itemslist:
                            if x['name'] == a['project name'] :
                                duplicate = True
                                break
                        if duplicate == False:
                            x = { "project name": x['name'], "state": x['state'],"goal": x['goal'], "pledged":x['pledged'], "city":x['location']["name"], "creator-id": x['creator']["id"],"creator": x['creator']["name"], "category": x["category"]["name"], "backers":x["backers_count"], "duration":x["deadline"] - x["launched_at"],  "Link": x['urls']["web"]["project"]}
                            temp_list.append(x)
                    except Exception as e:
                        print("This is an error message!", e)
    print('this is the temporary list for file', filename, temp_list ,len(temp_list), '\n')           
    return temp_list

itemslist = []
#get all files from folder
for filename in os.listdir(os.getcwd()):
    #print("before adding from file", filename, "itemslist is: \n\t", itemslist)
    itemslist.extend(get_items_from_file(filename))
                        
with builtins.open('results.json','w') as outfile:
    final_list = itemslist
    print('final list' ,len(final_list))
    json.dump(final_list, outfile)
#np.savetxt('data.csv', (col1_array, col2_array, col3_array), delimiter=',')


# Converting json into CSV that can be used further on in a database.
with open('results.json','r') as file:
    print(file)
    #df = pd.read_json(file)
    #print(df)
    #df.to_csv('above100k.csv',index=False)